$(document).ready(function(){
    $('.slider').slick({
      dots: true,
      infinite: true,
      speed: 500,
        fade: true,
      cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 2000
});
        $('.responsive').slick({
      infinite: false,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 4,
      responsive: [
        {
        breakpoint: 1024,
        settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
      }
    },
    {
        breakpoint: 768,
        settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
        breakpoint: 480,
        settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
});